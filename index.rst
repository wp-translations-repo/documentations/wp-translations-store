.. WP-Translations Store documentation master file, created by
   sphinx-quickstart on Tue Feb 13 21:34:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: _static/logo.jpg

Welcome to our documentation!
========

Table of contents

.. toctree::
  :caption: Licenses
  :maxdepth: 2

  Site/manage
  Site/renew
  Site/upgrade

.. toctree::
  :caption: Client
  :maxdepth: 2

  Client/features
  Client/installation
  Client/configuration
  Client/changelog

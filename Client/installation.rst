Installation
=========

Automated Installation
------------------

Installation is free, quick, and easy. Install WP-Translations from our site in minutes. #todo

Manual Installation
------------

From your WordPress Dashboard

1. Download your WP-Translations plugin to your desktop via the link available in your email receipt.
2. Install and activate the extension by clicking on "Plugins" > "Add" in your WordPress dashboard.
3. Enter and activate your license in the menu "WPT Pro".
4. Your plugin/theme is now translated and ready to receive the next updates of its translations!

Via your favorite FTP application to your server

1. Download your WP-Translations plugin to your desktop via the link available in your email receipt.
2. Extract the zip WP-Translations plugin folder to your desktop.
3. With your FTP program, upload the Plugin folder to the wp-content/plugins folder in your WordPress directory online.
4. Go to Plugins screen and find the WP-Translations plugin in the list.
5. Click Activate to activate it.
6. Enter and activate your license in the menu "WPT Pro".
7. Your plugin/theme is now translated and ready to receive the next updates of its translations!